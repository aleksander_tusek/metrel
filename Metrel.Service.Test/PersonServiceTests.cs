﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Metrel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using Metrel.Repository;
using System.ComponentModel.DataAnnotations;

namespace Metrel.Service.Tests
{
    [TestClass()]
    public class PersonServiceTests
    {
        IPersonRepository personRepository;
        IPersonService personService;

        [TestInitialize()]
        public void Initialize()
        {
            personRepository = A.Fake<IPersonRepository>();
            personService = new Service.PersonService(personRepository);
        } 

        [TestMethod()]
        [ExpectedException(typeof(ValidationException))]
        public void AddPersonTest_Throws_ValidationException_When_Firstname_Is_Empty()
        {
            var person = new Model.Person()
            {
                Firstname = "",
                Lastname = "Priimek",
            };

            personService.AddPerson(person);
        }

        [TestMethod()]
        [ExpectedException(typeof(ValidationException))]
        public void AddPersonTest_Throws_ValidationException_When_Lastname_Is_Empty()
        {
            var person = new Model.Person()
            {
                Firstname = "Ime",
                Lastname = "",
            };

            personService.AddPerson(person);
        }

        [TestMethod()]
        [ExpectedException(typeof(ValidationException))]
        public void AddPersonTest_Throws_ValidationException_When_TaxNumber_Format_Is_Invalid()
        {
            var person = new Model.Person()
            {
                Firstname = "Ime",
                Lastname = "Priimek",
                TaxNumber = "abcdefgh"
            };

            personService.AddPerson(person);
        }
    }
}