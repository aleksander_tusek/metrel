﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metrel.Repository
{
    public static class Mapper
    {
        public static Model.Person ToModel(this Data.Sqlite.Person entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new Model.Person()
            {
                Id = entity.Id,
                Firstname = entity.Firstname,
                Lastname = entity.Lastname,
                TaxNumber = entity.TaxNumber,
                HomeAddress = entity.Address.ToModel()
                //HomeAddress = new Model.Address()
                //{
                //    AddressLine1 = entity.AddressLine1,
                //    AddressLine2 = entity.AddressLine2,
                //    City = entity.City,
                //    ZipPostalCode = entity.ZipPostalCode,
                //    StateProvinceRegion = entity.StateProvinceRegion,
                //    Country = entity.Country
                //}                
            };
        }
        public static Model.Address ToModel(this Data.Sqlite.Address entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new Model.Address()
            {
                PersonId = entity.PersonId,
                AddressLine1 = entity.AddressLine1,
                AddressLine2 = entity.AddressLine2,
                City = entity.City,
                ZipPostalCode = entity.ZipPostalCode,
                StateProvinceRegion = entity.StateProvinceRegion,
                Country = entity.Country
            };
        }


        public static Data.Sqlite.Person ToEntity(this Model.Person model)
        {
            if (model == null)
            {
                return null;
            }

            return new Data.Sqlite.Person
            {
                Id = model.Id,
                Firstname = model.Firstname,
                Lastname = model.Lastname,
                TaxNumber = model.TaxNumber,
                Address = model.HomeAddress.ToEntity()
                //AddressLine1 = model.HomeAddress.AddressLine1,
                //AddressLine2 = model.HomeAddress.AddressLine2,
                //City = model.HomeAddress.City,
                //StateProvinceRegion = model.HomeAddress.StateProvinceRegion,
                //ZipPostalCode = model.HomeAddress.ZipPostalCode,
                //Country = model.HomeAddress.Country,
            };
        }

        public static Data.Sqlite.Address ToEntity(this Model.Address model)
        {
            if (model == null)
            {
                return null;
            }

            return new Data.Sqlite.Address
            {
                PersonId = model.PersonId,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                City = model.City,
                StateProvinceRegion = model.StateProvinceRegion,
                ZipPostalCode = model.ZipPostalCode,
                Country = model.Country,
            };
        }
    }
}
