﻿
using Metrel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metrel.Repository
{
    public interface IPersonRepository 
    {
        //IQueryable<Person> All { get; }
        Model.Person Find(long id);
        IEnumerable<Model.Person> Select();
        Model.Person Add(Person person);
        Model.Person Update(Person person);
        Model.Person Remove(long id);
    }
}
