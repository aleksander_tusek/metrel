﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using Metrel.Data.Sqlite;

namespace Metrel.Repository
{
    public class PersonRepository : IPersonRepository
    {
        public IEnumerable<Model.Person> Select()
        {
            using (var ctx = new metrelEntities())
            {
                return ctx.People
                        .AsEnumerable()
                        .Select(x => x.ToModel())
                        .ToList();
            }
        }

        public Model.Person Find(long id)
        {
            using (var ctx = new metrelEntities())
            {
                var entity = ctx.People.Find(id);

                ctx.Entry(entity).Reference(x => x.Address).Load();

                return entity.ToModel();
            }
        }

        public Model.Person Add(Model.Person person)
        {
            using (var ctx = new metrelEntities())
            {
                var entity = ctx.People.Add(person.ToEntity());

                ctx.SaveChanges();

                return entity.ToModel();
            }
        }

        public Model.Person Remove(long id)
        {
            using (var ctx = new metrelEntities())
            {
                var entity = ctx.People.Find(id);

                ctx.People.Remove(entity);

                ctx.SaveChanges();

                return entity.ToModel();
            }
        }

        public Model.Person Update(Model.Person person)
        {
            using (var ctx = new metrelEntities())
            {
                var entity = ctx.People.Find(person.Id);

                entity.Firstname = person.Firstname;
                entity.Lastname = person.Lastname;
                entity.TaxNumber = person.TaxNumber;

                if (person.HomeAddress != null)
                {
                    var address = entity.Address;

                    if (address == null)
                    {
                        address = new Address() { PersonId = person.Id };
                    }

                    address.AddressLine1 = person.HomeAddress.AddressLine1;
                    address.AddressLine2 = person.HomeAddress.AddressLine2;
                    address.City = person.HomeAddress.City;
                    address.ZipPostalCode = person.HomeAddress.ZipPostalCode;
                    address.StateProvinceRegion = person.HomeAddress.StateProvinceRegion;
                    address.Country = person.HomeAddress.Country;
                }

                ctx.SaveChanges();

                return entity.ToModel();
            }
        }
    }
}
