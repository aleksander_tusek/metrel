//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Metrel.Data.Sqlite
{
    using System;
    using System.Collections.Generic;
    
    public partial class Address
    {
        public long PersonId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateProvinceRegion { get; set; }
        public string ZipPostalCode { get; set; }
        public string Country { get; set; }
    
        public virtual Person Person { get; set; }
    }
}
