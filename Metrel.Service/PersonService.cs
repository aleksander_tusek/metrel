﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metrel.Model;
using Metrel.Repository;
using System.ComponentModel.DataAnnotations;

namespace Metrel.Service
{
    public class PersonService : IPersonService
    {
        private IPersonRepository personRepository;

        public PersonService()
        {
            this.personRepository = new PersonRepository();
        }

        internal PersonService(IPersonRepository personRepository)
        {
            this.personRepository = personRepository;
        }

        public Person AddPerson(Person person)
        {
            Validator.ValidateObject(person, new ValidationContext(person), validateAllProperties: true);

            return personRepository.Add(person);
        }

        public Person Find(int id)
        {
            return personRepository.Find(id);
        }

        public IEnumerable<Person> GetPersons()
        {
            return personRepository.Select();
        }

        public Person RemovePerson(int id)
        {
            return personRepository.Remove(id);
        }

        public Person UpdatePerson(Person person)
        {
            Validator.ValidateObject(person, new ValidationContext(person), validateAllProperties: true);

            return personRepository.Update(person);
        }
    }
}
