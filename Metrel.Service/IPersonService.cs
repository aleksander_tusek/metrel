﻿using Metrel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metrel.Service
{
    public interface IPersonService
    {
        IEnumerable<Person> GetPersons();
        Person Find(int id);
        Person AddPerson(Model.Person person);
        Person UpdatePerson(Model.Person person);
        Person RemovePerson(int id);
    }
}
