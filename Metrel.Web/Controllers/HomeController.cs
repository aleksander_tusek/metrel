﻿using Metrel.Model;
using Metrel.Service;
using Metrel.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Metrel.Web.Controllers
{
    public class HomeController : Controller
    {
        private IPersonService personService;

        public HomeController()
        {
            this.personService = new PersonService();
        }

        internal HomeController(IPersonService personService)
        {
            this.personService = personService;
        }

        public ActionResult Index()
        {
           return RedirectToAction("Index", "Person");
        }

        public ActionResult Persons(string sort, string sortdir)
        {
            var model = new HomeModel();

            model.Persons = personService.GetPersons();

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult PostalCodes(string search)
        //{
        //    var postalCodes = Helpers.PostalCodes;

        //    var res = postalCodes.Select(x => new { code = x.Key, name = x.Value })
        //                .Where(x => x.code.StartsWith(search))
        //                .Take(5);

        //    return Json(res,  JsonRequestBehavior.AllowGet);
        //}
    }
}