﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Metrel.Model;
using Metrel.Web.Models;
using Metrel.Service;
using System.ComponentModel.DataAnnotations;

namespace Metrel.Web.Controllers
{
    public class PersonController : Controller
    {
        private IPersonService personService;

        public PersonController()
        {
            this.personService = new PersonService();
        }

        internal PersonController(IPersonService personService)
        {
            this.personService = personService;
        }

        // GET: Person
        public ActionResult Index()
        {
            return View(personService.GetPersons());
        }

        // GET: Person/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Person person = personService.Find(id.Value);

            if (person == null)
            {
                return HttpNotFound();
            }

            return View(person);
        }

        // GET: Person/Create
        public ActionResult Create()
        {
            return View(new Person());
        }

        // POST: Person/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Firstname,Lastname,TaxNumber,HomeAddress")] Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    personService.AddPerson(person);

                    return RedirectToAction("Index");
                }
                catch (ValidationException e)
                {
                    ModelState.AddModelError(String.Join(",", e.ValidationResult.MemberNames), e.ValidationResult.ErrorMessage);
                }
            }

            return View(person);
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Person person = personService.Find(id.Value);

            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Person/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Firstname,Lastname,TaxNumber,HomeAddress")] Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    personService.UpdatePerson(person);

                    return RedirectToAction("Index");
                }
                catch (ValidationException e)
                {
                    ModelState.AddModelError(String.Join(",", e.ValidationResult.MemberNames), e.ValidationResult.ErrorMessage);
                }
            }
            return View(person);
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = personService.Find(id.Value);

            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Person/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            personService.RemovePerson(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            //    db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
