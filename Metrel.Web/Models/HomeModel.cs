﻿using Metrel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metrel.Web.Models
{
    public class HomeModel
    {
        public IEnumerable<Person> Persons { get; set; }
    }
}