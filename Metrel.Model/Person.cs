﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Metrel.Model
{
    public class Person
    {
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Firstname { get; set; }

        [Required]
        [StringLength(100)]
        public string Lastname { get; set; }

        [RegularExpression(@"^\d{8}$")]
        public string TaxNumber { get; set; }

        public Address HomeAddress { get; set; }

       
    }
}
