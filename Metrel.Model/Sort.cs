﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metrel.Model
{
    public class GetParams
    {
        public string Sort { get; set; }

        public string SortDir { get; set; }

        public int Page { get; set; }

        public int ItemsPerPage { get; set; }
    }
}
